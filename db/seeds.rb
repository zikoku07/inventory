# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


department = Department.create(name: 'Head Office', description: Faker::Lorem.sentences, address: Faker::Address.secondary_address)

Employee.create(email: 'zikoku07@gmail.com', password: '123456', name: 'Admin Name', gender: 'Male', phone: '098765432', is_active: true, address: 'Gulshan', role: 'Admin', department_id: department.id)
(1..5).each do

  ################## Product Category #######################
  product_category = department.product_categories.create(name: Faker::Commerce.department(2), description: Faker::Lorem.sentence)


  ################## Expense Category #######################
  expense_category = department.expenses_categories.create(name: Faker::Commerce.department(2), description: Faker::Lorem.sentence)


  (1..5).each do

    ################## Expenses #######################
    department.expenses.create(category_id: expense_category.id, amount: Faker::Number.number(4), note: Faker::Lorem.sentences, date: Faker::Date.between(Date.today.beginning_of_year, Date.today.end_of_year))

    ################## Product Sub Category #######################
    product_sub_category = department.product_categories.create(name: Faker::Lorem.word, description: Faker::Lorem.sentence, category_id: product_category.id)
    (1..2).each do

      ################## Product #######################
      department.products.create(name: Faker::Commerce.product_name, code: Faker::Number.number(8), buying_price: Faker::Commerce.price, selling_price: Faker::Commerce.price, quantity: Faker::Number.number(2), note: Faker::Lorem.sentences, category_id: product_category.id, sub_category_id: product_sub_category.id)
    end
  end
  department.parties.create(name: Faker::Name.name, company: Faker::Company.name, email: Faker::Internet.email, phone: Faker::PhoneNumber.phone_number, address: Faker::Address.secondary_address, is_active: true, department_id: department.id)
  # department.suppliers.create(name: Faker::Name.name, company: Faker::Company.name, email: Faker::Internet.email, phone: Faker::PhoneNumber.phone_number, address: Faker::Address.secondary_address, is_active: true, department_id: department.id)
  # department.customers.create(name: Faker::Name.name, company: Faker::Company.name, email: Faker::Internet.email, phone: Faker::PhoneNumber.phone_number, address: Faker::Address.secondary_address, is_active: true, department_id: department.id)
end
