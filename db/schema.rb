# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170302052422) do

  create_table "daily_closings", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.date     "date"
    t.float    "balance",       limit: 24
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.text     "address",     limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.float    "balance",     limit: 24,    default: 0.0
  end

  create_table "employees", force: :cascade do |t|
    t.string   "email",                  limit: 255,   default: "",   null: false
    t.string   "encrypted_password",     limit: 255,   default: "",   null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "user_name",              limit: 255
    t.string   "name",                   limit: 255
    t.integer  "department_id",          limit: 4
    t.string   "gender",                 limit: 255
    t.string   "role",                   limit: 255
    t.string   "phone",                  limit: 255
    t.string   "image",                  limit: 255
    t.boolean  "is_active",                            default: true
    t.text     "address",                limit: 65535
  end

  add_index "employees", ["email"], name: "index_employees_on_email", unique: true, using: :btree
  add_index "employees", ["reset_password_token"], name: "index_employees_on_reset_password_token", unique: true, using: :btree

  create_table "expenses_categories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.text     "description",   limit: 65535
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "expenses_expenses", force: :cascade do |t|
    t.integer  "category_id",   limit: 4
    t.float    "amount",        limit: 24
    t.text     "note",          limit: 65535
    t.date     "date"
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "inventory_customers_customers", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "image",         limit: 255
    t.string   "company",       limit: 255
    t.string   "email",         limit: 255
    t.string   "phone",         limit: 255
    t.string   "address",       limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "zip_code",      limit: 255
    t.string   "country",       limit: 255
    t.text     "note",          limit: 65535
    t.boolean  "is_active",                   default: true
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "inventory_customers_order_items", force: :cascade do |t|
    t.integer  "order_id",      limit: 4
    t.integer  "product_id",    limit: 4
    t.float    "buying_price",  limit: 24
    t.string   "selling_price", limit: 255
    t.integer  "quantity",      limit: 4
    t.float    "vat",           limit: 24
    t.float    "discount",      limit: 24
    t.float    "total",         limit: 24
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.text     "description",   limit: 65535
  end

  create_table "inventory_customers_orders", force: :cascade do |t|
    t.integer  "customer_id",   limit: 4
    t.date     "date"
    t.date     "delivery_date"
    t.float    "price",         limit: 24
    t.float    "discount",      limit: 24
    t.float    "vat",           limit: 24
    t.float    "total",         limit: 24
    t.integer  "department_id", limit: 4
    t.text     "note",          limit: 65535
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "party_id",      limit: 4
    t.boolean  "is_cash",                     default: false
    t.integer  "employee_id",   limit: 4
  end

  create_table "inventory_customers_payments", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.integer  "party_id",      limit: 4
    t.integer  "employee_id",   limit: 4
    t.integer  "order_id",      limit: 4
    t.float    "amount",        limit: 24
    t.float    "discount",      limit: 24
    t.date     "date"
    t.string   "method",        limit: 255
    t.string   "bank_name",     limit: 255
    t.string   "cheque_no",     limit: 255
    t.date     "value_date"
    t.text     "note",          limit: 65535
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.float    "total",         limit: 24
    t.boolean  "is_cash",                     default: false
  end

  create_table "inventory_deposit_categories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.text     "description",   limit: 65535
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "inventory_deposits", force: :cascade do |t|
    t.integer  "department_id",       limit: 4
    t.integer  "deposit_category_id", limit: 4
    t.float    "amount",              limit: 24
    t.date     "date"
    t.text     "note",                limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "inventory_parties", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "image",               limit: 255
    t.string   "company",             limit: 255
    t.string   "email",               limit: 255
    t.string   "phone",               limit: 255
    t.string   "address",             limit: 255
    t.string   "city",                limit: 255
    t.string   "state",               limit: 255
    t.string   "zip_code",            limit: 255
    t.string   "country",             limit: 255
    t.text     "note",                limit: 65535
    t.boolean  "is_active",                         default: true
    t.integer  "department_id",       limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.float    "in_balance",          limit: 24,    default: 0.0
    t.float    "out_balance",         limit: 24,    default: 0.0
    t.float    "initial_in_balance",  limit: 24,    default: 0.0
    t.float    "initial_out_balance", limit: 24,    default: 0.0
  end

  create_table "inventory_product_categories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.text     "description",   limit: 65535
    t.integer  "category_id",   limit: 4
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "inventory_product_replacements", force: :cascade do |t|
    t.integer  "party_id",      limit: 4
    t.integer  "department_id", limit: 4
    t.integer  "product_id",    limit: 4
    t.date     "date"
    t.date     "receive_date"
    t.integer  "quantity",      limit: 4
    t.text     "note",          limit: 65535
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "tracking_no",   limit: 255
  end

  create_table "inventory_products", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "code",             limit: 255
    t.float    "buying_price",     limit: 24
    t.float    "selling_price",    limit: 24
    t.integer  "quantity",         limit: 4,     default: 0
    t.text     "note",             limit: 65535
    t.integer  "department_id",    limit: 4
    t.integer  "category_id",      limit: 4
    t.integer  "sub_category_id",  limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.boolean  "is_referenced",                  default: false
    t.integer  "initial_quantity", limit: 4,     default: 0
  end

  create_table "inventory_suppliers_order_items", force: :cascade do |t|
    t.integer  "order_id",     limit: 4
    t.integer  "product_id",   limit: 4
    t.float    "price",        limit: 24
    t.integer  "quantity",     limit: 4
    t.float    "vat",          limit: 24
    t.float    "discount",     limit: 24
    t.float    "total",        limit: 24
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "description",  limit: 65535
    t.float    "buying_price", limit: 24
  end

  create_table "inventory_suppliers_orders", force: :cascade do |t|
    t.integer  "supplier_id",   limit: 4
    t.date     "date"
    t.date     "receive_date"
    t.float    "price",         limit: 24
    t.float    "discount",      limit: 24
    t.float    "vat",           limit: 24
    t.float    "total",         limit: 24
    t.integer  "department_id", limit: 4
    t.text     "note",          limit: 65535
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.date     "delivery_date"
    t.string   "terms",         limit: 255
    t.string   "via",           limit: 255
    t.integer  "party_id",      limit: 4
    t.string   "method",        limit: 255,   default: "Cash"
    t.boolean  "is_complete",                 default: false
    t.boolean  "is_cash",                     default: false
    t.integer  "employee_id",   limit: 4
  end

  create_table "inventory_suppliers_payments", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.integer  "party_id",      limit: 4
    t.integer  "employee_id",   limit: 4
    t.integer  "order_id",      limit: 4
    t.float    "amount",        limit: 24
    t.float    "discount",      limit: 24
    t.date     "date"
    t.string   "method",        limit: 255
    t.string   "bank_name",     limit: 255
    t.string   "cheque_no",     limit: 255
    t.date     "value_date"
    t.text     "note",          limit: 65535
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.float    "total",         limit: 24
    t.boolean  "is_cash",                     default: false
  end

  create_table "inventory_suppliers_suppliers", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "image",         limit: 255
    t.string   "company",       limit: 255
    t.string   "email",         limit: 255
    t.string   "phone",         limit: 255
    t.string   "address",       limit: 255
    t.string   "city",          limit: 255
    t.string   "state",         limit: 255
    t.string   "zip_code",      limit: 255
    t.string   "country",       limit: 255
    t.text     "note",          limit: 65535
    t.boolean  "is_active",                   default: true
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "inventory_withdraw_categories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.text     "description",   limit: 65535
    t.integer  "department_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "inventory_withdraws", force: :cascade do |t|
    t.integer  "department_id",        limit: 4
    t.integer  "withdraw_category_id", limit: 4
    t.float    "amount",               limit: 24
    t.date     "date"
    t.text     "note",                 limit: 65535
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "parties_monthly_closings", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.integer  "party_id",      limit: 4
    t.integer  "balance",       limit: 4
    t.integer  "month",         limit: 4
    t.integer  "year",          limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "products_monthly_closings", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.integer  "product_id",    limit: 4
    t.integer  "quantity",      limit: 4
    t.float    "buying_price",  limit: 24
    t.integer  "month",         limit: 4
    t.integer  "year",          limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

end
