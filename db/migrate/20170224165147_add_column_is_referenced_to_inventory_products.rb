class AddColumnIsReferencedToInventoryProducts < ActiveRecord::Migration
  def change
    add_column :inventory_products, :is_referenced, :boolean, default: false
  end
end
