class AddIsCashFieldToInventorySuppliersOrders < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_orders, :is_cash, :boolean, default: false
    add_column :inventory_suppliers_orders, :employee_id, :integer
    add_column :inventory_suppliers_payments, :is_cash, :boolean, default: false
  end
end
