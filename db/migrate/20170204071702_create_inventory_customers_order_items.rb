class CreateInventoryCustomersOrderItems < ActiveRecord::Migration
  def change
    create_table :inventory_customers_order_items do |t|
      t.integer :order_id
      t.integer :product_id
      t.float :buying_price
      t.string :selling_price
      t.integer :quantity
      t.float :vat
      t.float :discount
      t.float :total

      t.timestamps null: false
    end
  end
end
