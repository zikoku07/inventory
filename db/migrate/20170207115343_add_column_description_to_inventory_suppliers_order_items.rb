class AddColumnDescriptionToInventorySuppliersOrderItems < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_order_items, :description, :text
  end
end
