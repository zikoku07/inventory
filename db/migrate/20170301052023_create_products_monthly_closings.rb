class CreateProductsMonthlyClosings < ActiveRecord::Migration
  def change
    create_table :products_monthly_closings do |t|
      t.integer :department_id
      t.integer :product_id
      t.integer :quantity
      t.float :buying_price
      t.integer :month
      t.integer :year
      t.timestamps null: false
    end
  end
end
