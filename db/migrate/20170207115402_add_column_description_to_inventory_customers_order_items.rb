class AddColumnDescriptionToInventoryCustomersOrderItems < ActiveRecord::Migration
  def change
    add_column :inventory_customers_order_items, :description, :text
  end
end
