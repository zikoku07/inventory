class CreateDailyClosings < ActiveRecord::Migration
  def change
    create_table :daily_closings do |t|
      t.integer :department_id
      t.date :date
      t.float :balance
      t.timestamps null: false
    end
  end
end
