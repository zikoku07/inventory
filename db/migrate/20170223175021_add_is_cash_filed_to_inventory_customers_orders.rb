class AddIsCashFiledToInventoryCustomersOrders < ActiveRecord::Migration
  def change
    add_column :inventory_customers_orders, :is_cash, :boolean, default: false
    add_column :inventory_customers_orders, :employee_id, :integer
    add_column :inventory_customers_payments, :is_cash, :boolean, default: false

  end
end
