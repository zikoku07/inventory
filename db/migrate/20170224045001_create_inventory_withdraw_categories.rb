class CreateInventoryWithdrawCategories < ActiveRecord::Migration
  def change
    create_table :inventory_withdraw_categories do |t|
      t.string :name
      t.text :description
      t.integer :department_id
      t.timestamps null: false
    end
  end
end
