class AddColumnDeliveryDateToInventorySuppliersOrders < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_orders, :delivery_date, :date
    add_column :inventory_suppliers_orders, :terms, :string
    add_column :inventory_suppliers_orders, :via, :string
    add_column :inventory_suppliers_orders, :method, :string
  end
end
