class AddColumnPartyIdToInventorySuppliersOrders < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_orders, :party_id, :integer
  end
end
