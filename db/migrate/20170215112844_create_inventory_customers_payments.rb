class CreateInventoryCustomersPayments < ActiveRecord::Migration
  def change
    create_table :inventory_customers_payments do |t|
      t.integer :department_id
      t.integer :party_id
      t.integer :employee_id
      t.integer :order_id
      t.float :amount
      t.float :discount
      t.date :date
      t.string :method
      t.string :bank_name
      t.string :cheque_no
      t.date :value_date
      t.text :note
      t.timestamps null: false
    end
  end
end
