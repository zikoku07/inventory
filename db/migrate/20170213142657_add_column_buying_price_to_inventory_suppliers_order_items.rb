class AddColumnBuyingPriceToInventorySuppliersOrderItems < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_order_items, :buying_price, :float
  end
end
