class AddColumnTrackingNoToInventoryProductReplacements < ActiveRecord::Migration
  def change
    add_column :inventory_product_replacements, :tracking_no, :string
  end
end
