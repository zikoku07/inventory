class AddColumnPartyIdToInventoryCustomersOrders < ActiveRecord::Migration
  def change
    add_column :inventory_customers_orders, :party_id, :integer
  end
end
