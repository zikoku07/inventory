class CreateInventorySuppliersOrders < ActiveRecord::Migration
  def change
    create_table :inventory_suppliers_orders do |t|
      t.integer :supplier_id
      t.date :date
      t.date :receive_date
      t.float :price
      t.float :discount
      t.float :vat
      t.float :total
      t.integer :department_id
      t.text :note

      t.timestamps null: false
    end
  end
end
