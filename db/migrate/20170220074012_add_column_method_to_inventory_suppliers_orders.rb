class AddColumnMethodToInventorySuppliersOrders < ActiveRecord::Migration
  def change
    remove_column :inventory_suppliers_orders, :method
    add_column :inventory_suppliers_orders, :method, :string, default: AppSetting::TRANSACTION_TYPES[:cash]
    add_column :inventory_suppliers_orders, :is_complete, :boolean, default: false
  end
end
