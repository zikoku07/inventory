class AddColumnBalanceToInventoryParties < ActiveRecord::Migration
  def change
    add_column :inventory_parties, :in_balance, :float, default: 0.00
    add_column :inventory_parties, :out_balance, :float, default: 0.00
  end
end
