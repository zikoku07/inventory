class CreateInventoryWithdraws < ActiveRecord::Migration
  def change
    create_table :inventory_withdraws do |t|
      t.integer :department_id
      t.integer :withdraw_category_id
      t.float :amount
      t.date :date
      t.text :note
      t.timestamps null: false
    end
  end
end
