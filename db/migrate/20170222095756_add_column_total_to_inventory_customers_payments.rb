class AddColumnTotalToInventoryCustomersPayments < ActiveRecord::Migration
  def change
    add_column :inventory_customers_payments, :total, :float
  end
end
