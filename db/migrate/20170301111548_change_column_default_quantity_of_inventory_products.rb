class ChangeColumnDefaultQuantityOfInventoryProducts < ActiveRecord::Migration
  def change
    change_column_default :inventory_products, :quantity, 0
  end
end
