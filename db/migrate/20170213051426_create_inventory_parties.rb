class CreateInventoryParties < ActiveRecord::Migration
  def change
    create_table :inventory_parties do |t|
      t.string :name
      t.string :image
      t.string :company
      t.string :email
      t.string :phone
      t.string :address
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :country
      t.text :note
      t.boolean :is_active, default: true
      t.integer :department_id
      t.timestamps null: false
    end
  end
end
