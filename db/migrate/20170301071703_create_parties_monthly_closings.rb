class CreatePartiesMonthlyClosings < ActiveRecord::Migration
  def change
    create_table :parties_monthly_closings do |t|
      t.integer :department_id
      t.integer :party_id
      t.integer :balance
      t.integer :month
      t.integer :year
      t.timestamps null: false
    end
  end
end
