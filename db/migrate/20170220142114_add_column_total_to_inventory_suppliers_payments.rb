class AddColumnTotalToInventorySuppliersPayments < ActiveRecord::Migration
  def change
    add_column :inventory_suppliers_payments, :total, :float
  end
end
