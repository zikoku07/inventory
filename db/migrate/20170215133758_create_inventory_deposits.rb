class CreateInventoryDeposits < ActiveRecord::Migration
  def change
    create_table :inventory_deposits do |t|
      t.integer :department_id
      t.integer :deposit_category_id
      t.float :amount
      t.date :date
      t.text :note
      t.timestamps null: false
    end
  end
end
