class AddColumnBalanceToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :balance, :float, default: 0.0
  end
end
