class CreateInventoryProductCategories < ActiveRecord::Migration
  def change
    create_table :inventory_product_categories do |t|
      t.string :name
      t.text :description
      t.references :category
      t.references :department
      t.timestamps null: false
    end
  end
end
