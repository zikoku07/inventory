class CreateProducts < ActiveRecord::Migration
  def change
    create_table :inventory_products do |t|
      t.string :name
      t.string :code
      t.float :buying_price
      t.float :selling_price
      t.integer :quantity
      t.text :note
      t.integer :department_id
      t.integer :category_id
      t.integer :sub_category_id

      t.timestamps null: false
    end
  end
end
