class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses_expenses do |t|
      t.integer :category_id
      t.float :amount
      t.text :note
      t.date :date
      t.integer :department_id

      t.timestamps null: false
    end
  end
end
