class AddColumnUserNameToEmployees < ActiveRecord::Migration
  def change
    add_column :employees, :user_name, :string
    add_column :employees, :name, :string
    add_column :employees, :department_id, :integer
    add_column :employees, :gender, :string
    add_column :employees, :role, :string
    add_column :employees, :phone, :string
    add_column :employees, :image, :string
    add_column :employees, :is_active, :boolean, default: true
    add_column :employees, :address, :text
  end
end
