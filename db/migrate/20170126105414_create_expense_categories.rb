class CreateExpenseCategories < ActiveRecord::Migration
  def change
    create_table :expenses_categories do |t|
      t.string :name
      t.text :description
      t.integer :department_id

      t.timestamps null: false
    end
  end
end
