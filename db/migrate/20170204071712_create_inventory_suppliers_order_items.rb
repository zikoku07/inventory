class CreateInventorySuppliersOrderItems < ActiveRecord::Migration
  def change
    create_table :inventory_suppliers_order_items do |t|
      t.integer :order_id
      t.integer :product_id
      t.float :price
      t.integer :quantity
      t.float :vat
      t.float :discount
      t.float :total

      t.timestamps null: false
    end
  end
end
