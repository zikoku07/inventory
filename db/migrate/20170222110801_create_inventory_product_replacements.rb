class CreateInventoryProductReplacements < ActiveRecord::Migration
  def change
    create_table :inventory_product_replacements do |t|
      t.integer :party_id
      t.integer :department_id
      t.integer :product_id
      t.date :date
      t.date :receive_date
      t.integer :quantity
      t.text :note
      t.timestamps null: false
    end
  end
end
