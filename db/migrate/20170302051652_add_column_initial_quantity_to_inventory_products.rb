class AddColumnInitialQuantityToInventoryProducts < ActiveRecord::Migration
  def change
    add_column :inventory_products, :initial_quantity, :integer, default: 0
  end
end
