class AddColumnInitialBalanceToInventoryParties < ActiveRecord::Migration
  def change
    add_column :inventory_parties, :initial_in_balance, :float, default: 0
    add_column :inventory_parties, :initial_out_balance, :float, default: 0
  end
end
