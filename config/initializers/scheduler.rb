require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new

scheduler.cron '0 0 1 * *' do
  puts 'Close monthly statements'
  ProductsMonthlyClosing.close
  PartiesMonthlyClosing.close
end

# scheduler.in '3s' do
#   puts 'Hello... Rufus'
#   ProductsMonthlyClosing.close
#   puts 'Close...Product... Rufus'
#   PartiesMonthlyClosing.close
#   puts 'Close...Party... Rufus'
# end