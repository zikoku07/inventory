Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  devise_for :employees

  # You can have the root of your site routed with "root"
  root 'home#cash_book'

  get '/daily_status', to: 'home#daily_status'
  get '/cash_book', to: 'home#cash_book'
  get '/close_previous_month', to: 'home#close_previous_month'

  resources :daily_closings

  resources :departments do
    get :switch, on: :member
    get :initial_balance, on: :member
    put :update_balance, on: :member
  end

  resources :employees do
    post :add, on: :collection
    put :update_info, on: :member
    get :change_password, on: :member
    put :update_password, on: :member
  end

  namespace :expenses do
    resources :categories
    resources :expenses do
      get :monthly_report, on: :collection
      get :yearly_report, on: :collection
      get :report_breakdown, on: :collection
    end
  end

  namespace :inventory do
    namespace :suppliers do
      resources :suppliers
      resources :orders
      resources :payments
    end
    namespace :customers do
      resources :customers
      resources :orders
      resources :payments
    end
    resources :parties do
      get :balance_report, on: :collection
      post :import, on: :collection
    end
    resources :products do
      get :get_details, on: :member
      get :stock_report, on: :collection
      post :import, on: :collection
    end
    resources :product_categories do
      get :product_sub_categories, on: :member
    end
    resources :deposit_categories
    resources :deposits
    resources :product_replacements do
      get :receive, on: :member
      get :reject_product, on: :member
    end
    resources :withdraw_categories
    resources :withdraws
  end
end
