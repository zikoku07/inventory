// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
// require turbolinks
//= require chosen-jquery
//= require moment
//= require_tree .

function popupMessage(message, klass) {
    notificationTop = "+" + ($(document).scrollTop() + 60);
    $('#notification').removeClass().addClass('alert '+ klass );
    $('#flash-msg-text').html(message);
    $('#notification').show().animate({
        top: notificationTop
    }, 200);
    $('#notice-close').click(function(){
        $('#notification').hide();
    });
    setTimeout(function () {
        $('#notification').hide().animate({
            top: "-60"
        }, 500);
    }, 4000);
}

$(function () {
    $('.custom-chosen').chosen({
        width: "100%"
    });
    $('.submit-on-change').on('change', function () {
        if ($(this).val() != '') {
            $(this).parent('form').submit();
        }
    });

    $('.submit-on-change-with-all').on('change', function () {
        //if($(this).val() != '') {
        $(this).parent('form').submit();
        //}
    });

    $('.pagesize').on('click', function () {
        $(this).parents('.paging-perpage').find('a').removeClass('active');
        $(this).addClass('active');
    });

    $("[href='#']").click(function () {
        return false;
    });
});
