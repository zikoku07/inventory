module Expenses
  class Expense < Base
    belongs_to :department
    belongs_to :expenses_category, :class_name => 'Expenses::Category', foreign_key: :category_id

    def self.get_expense_report(categories, current_department, start_date, end_date, is_monthly)
      expenses = current_department.expenses.where(date: start_date..end_date)
      start_number = 1
      end_number = 12
      if is_monthly
        start_number = start_date.day
        end_number = end_date.day
      end
      expense_report = initialize_expense_report(categories, start_number, end_number)
      expense_report = get_updated_expense_report(expenses, expense_report, is_monthly)
      expense_report
    end

    def self.initialize_expense_report(categories, start_number, end_number)
      expense_report = {}
      expense_report[:month_total] = {
          amount: 0
      }
      (start_number..end_number).each do |number|
        expense_report[:month_total][number] = 0
      end
      categories.each do |category|
        expense_report[category.id] = {
            category: category,
            total_amount: 0
        }
        (start_number..end_number).each do |number|
          month_report = {
              amount: 0
          }
          expense_report[category.id][number] = month_report
        end
      end
      expense_report
    end

    def self.get_updated_expense_report(expenses, expense_report, is_month)
      expenses.each do |expense|
        month_num = is_month ? expense.date.day.to_i : expense.date.month.to_i
        if expense.category_id.present? && expense_report[expense.category_id][month_num].present?
          expense_report[expense.category_id][month_num][:amount] += expense.amount
          expense_report[expense.category_id][:total_amount] += expense.amount
          expense_report[:month_total][month_num] += expense.amount
          expense_report[:month_total][:amount] += expense.amount
        end
      end
      expense_report
    end
  end
end
