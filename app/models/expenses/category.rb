module Expenses
  class Category < Base
    belongs_to :department
    has_many :expenses, :class_name => 'Expenses::Expense', foreign_key: :category_id
  end
end
