module Expenses
  class Base < ActiveRecord::Base
    self.abstract_class = true
    self.table_name_prefix = 'expenses_'
  end
end