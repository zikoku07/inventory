class AppSetting
  EMPLOYEE_ROLE = {
      admin: 'Admin',
      staff: 'Staff'
  }

  TRANSACTION_TYPES = {
      cash: 'Cash',
      credit: 'Credit'
  }

  PAYMENT_TYPES = {
      cash: 'Cash',
      cheque: 'Cheque'
  }

  MONTHS_SHORT_NAME = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
end