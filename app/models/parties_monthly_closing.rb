class PartiesMonthlyClosing < ActiveRecord::Base
  belongs_to :department
  belongs_to :party, :class_name => 'Inventory::Party'

  def self.close(month = (Date.today - 1.day).month, year = (Date.today - 1.day).year)
    department = Department.first
    Inventory::Party.all.each do |party|
      department.parties_monthly_closings.create(party_id: party.id, balance: (party.in_balance - party.out_balance), month: month, year: year)
    end
  end
end
