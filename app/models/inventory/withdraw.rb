module Inventory
  class Withdraw < Inventory::Base
    belongs_to :department
    belongs_to :withdraw_category, :class_name => 'Inventory::WithdrawCategory'
  end
end
