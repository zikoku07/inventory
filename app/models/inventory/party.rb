module Inventory
  class Party < Base
    belongs_to :department
    has_many :purchase_orders, :class_name => 'Inventory::Suppliers::Order'
    has_many :suppliers_payments, :class_name => 'Inventory::Suppliers::Payment'
    has_many :customers_payments, :class_name => 'Inventory::Customers::Payment'
    has_many :product_replacements, :class_name => 'Inventory::ProductReplacement'
    has_many :parties_monthly_closings

    def created_date
      self.created_at.present? ? self.created_at.strftime('%Y-%m-%d').to_date : Date.today
    end

    def self.active
      self.where(is_active: true)
    end

    def self.import(current_department, file)
      spreadsheet = Roo::Spreadsheet.open(file.path)
      header = spreadsheet.row(1)
      header[6] = 'initial_in_balance'
      header[7] = 'initial_out_balance'
      puts header.inspect

      (2..spreadsheet.last_row).each do |i|
        item = spreadsheet.row(i)
        item[6] = 0 unless item[6].present?
        item[7] = 0 unless item[7].present?
        row = Hash[[header, item].transpose]
        party = new
        party.attributes = row.to_hash
        party.department_id = current_department.id
        party.save!
      end
    end

    def self.history(current_department, party_id, start_date, end_date)
      history = []
      history = purchase_order_history(history, current_department, party_id, start_date, end_date)
      history = sales_order_history(history, current_department, party_id, start_date, end_date)
      history = supplier_payment_history(history, current_department, party_id, start_date, end_date)
      history = customer_payment_history(history, current_department, party_id, start_date, end_date)
      history.sort_by! {|hist| hist[:date]}
    end

    def self.purchase_order_history(history, current_department, party_id, start_date, end_date)
      purchase_orders = current_department.purchase_orders.where(party_id: party_id, date: start_date..end_date).includes(:party)
      purchase_orders.each do |order|
        history.push({party: order.party.name, type: 'Purchase order', amount_plus: order.total, amount_minus: 0, date: order.created_at})
      end
      history
    end

    def self.sales_order_history(history, current_department, party_id, start_date, end_date)
      sales_orders = current_department.sale_orders.where(party_id: party_id, date: start_date..end_date).includes(:party)
      sales_orders.each do |order|
        history.push({party: order.party.name, type: 'Sale order', amount_plus: 0, amount_minus: order.total, date: order.created_at})
      end
      history
    end

    def self.supplier_payment_history(history, current_department, party_id, start_date, end_date)
      payments = current_department.suppliers_payments.where(party_id: party_id, date: start_date..end_date).includes(:party)
      payments.each do |payment|
        history.push({party: payment.party.name, type: 'Payment Given', amount_plus: 0, amount_minus: payment.amount, date: payment.created_at})
      end
      history
    end

    def self.customer_payment_history(history, current_department, party_id, start_date, end_date)
      payments = current_department.customers_payments.where(party_id: party_id, date: start_date..end_date).includes(:party)
      payments.each do |payment|
        history.push({party: payment.party.name, type: 'Payment Received', amount_plus: payment.amount, amount_minus: 0, date: payment.created_at})
      end
      history
    end
  end
end
