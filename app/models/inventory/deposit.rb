module Inventory
  class Deposit < Base
    belongs_to :department
    belongs_to :deposit_category, :class_name => 'Inventory::DepositCategory'
  end
end
