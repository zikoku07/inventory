module Inventory
  class ProductReplacement < Inventory::Base
    belongs_to :department
    belongs_to :party, :class_name => 'Inventory::Party'
    belongs_to :product, :class_name => 'Inventory::Product'
    after_save :change_product_quantity
    after_destroy :reset_product_quantity

    protected

    def change_product_quantity
      product = Inventory::Product.find_by_id(party_id)
      if quantity_was.present?
        if quantity_changed?
          product.update_attributes(quantity: (product.quantity + quantity_was) - quantity)
        end
      else
        product.update_attributes(quantity: product.quantity - quantity)
      end
    end

    def reset_product_quantity
      product = Inventory::Product.find_by_id(party_id)
      product.update_attributes(quantity: product.quantity + quantity)
    end
  end
end
