module Inventory
  class ProductCategory < Base
    belongs_to :department
    has_many :sub_categories, :class_name => 'Inventory::ProductCategory', foreign_key: :category_id
    belongs_to :category, :class_name => 'Inventory::ProductCategory', foreign_key: :category_id
    has_many :products, :class_name => 'Inventory::Product', foreign_key: :category_id
    has_many :sub_products, :class_name => 'Inventory::Product', foreign_key: :sub_category_id

    def self.get_all(current_department)
      current_department.product_categories.where(category_id: nil)
    end

    def self.sub_categories(category_id)
      where(category_id: category_id)
    end
  end
end
