module Inventory
  class Product < Base
    belongs_to :department
    belongs_to :category, :class_name => 'Inventory::ProductCategory', foreign_key: :category_id
    belongs_to :sub_category, :class_name => 'Inventory::ProductCategory', foreign_key: :sub_category_id
    has_many :product_replacements, :class_name => 'Inventory::ProductReplacement'
    has_many :customers_order_items, :class_name => 'Inventory::Customers::OrderItem'
    has_many :suppliers_order_items, :class_name => 'Inventory::Suppliers::OrderItem'
    has_many :products_monthly_closings

    before_save :set_initial_balance

    def created_date
      self.created_at.present? ? self.created_at.strftime('%Y-%m-%d').to_date : Date.today
    end

    def self.history(current_department, product_id, start_date, end_date)
      history = []
      history = purchase_order_history(history, current_department, product_id, start_date, end_date)
      history = sales_order_history(history, current_department, product_id, start_date, end_date)
      history = replacement_history(history, current_department, product_id, start_date, end_date)
      history.sort_by! {|hist| hist[:date]}
    end

    def self.purchase_order_history(history, current_department, product_id, start_date, end_date)
      purchase_order_items = Inventory::Suppliers::OrderItem.where(product_id: product_id).includes(:order).includes(:product).where('inventory_suppliers_orders.department_id = ? AND inventory_suppliers_orders.date IN (?)', current_department.id, start_date..end_date).references(:inventory_suppliers_orders)
      purchase_order_items.each do |order_item|
        #total_in += order_item.quantity
        history.push({product: order_item.product.name, type: 'Purchase order', in: order_item.quantity, out: 0, date: order_item.order.date})
      end
      history
    end

    def self.sales_order_history(history, current_department, product_id, start_date, end_date)
      sales_order_items = Inventory::Customers::OrderItem.where(product_id: product_id).includes(:order).includes(:product).where('inventory_customers_orders.department_id = ? AND inventory_customers_orders.date IN (?)', current_department.id, start_date..end_date).references(:inventory_customers_orders)
      sales_order_items.each do |order_item|
        #total_out += order_item.quantity
        history.push({product: order_item.product.name, type: 'Sales order', in: 0, out: order_item.quantity, date: order_item.order.date})
      end
      history
    end

    def self.replacement_history(history, current_department, product_id, start_date, end_date)
      replacements = current_department.product_replacements.where(product_id: product_id).includes(:product).where(date: start_date..end_date)

      replacements.each do |replacement|
        #total_out += replacement.quantity
        history.push({product: replacement.product.name, type: 'Replacement', in: 0, out: replacement.quantity, date: replacement.date})
        if replacement.receive_date.present? && replacement.receive_date.between?(start_date, end_date)
          #total_in += replacement.quantity
          history.push({product: replacement.product.name, type: 'Replacement received', in: replacement.quantity, out: 0, date: replacement.date})
        end
      end
      history
    end

    def self.import(file)
      spreadsheet = Roo::Spreadsheet.open(file.path)
      header = spreadsheet.row(1)
      header.delete_at(2)
      header.delete_at(2)
      (2..spreadsheet.last_row).each do |i|
        item = spreadsheet.row(i)
        item_full = spreadsheet.row(i)
        item[4] = 0 unless item[4].present?
        item[5] = 0 unless item[5].present?
        item.delete_at(2)
        item.delete_at(2)

        row = Hash[[header, item].transpose]
        product = new
        product.attributes = row.to_hash
        dept = Department.first
        product.department_id = dept.id
        product_category = Inventory::ProductCategory.find_or_initialize_by(name: item_full[2])
        if product_category.new_record?
          product_category.department_id = dept.id
          product_category.save!
        end

        product.category_id = product_category.id
        if item_full[3].present? && item_full[3].size > 0
          product_sub_category = Inventory::ProductCategory.find_or_initialize_by(name: item_full[3])
          if product_sub_category.new_record?
            product_sub_category.category_id = product_category.id
            product_sub_category.department_id = dept.id
            product_sub_category.save!
          end
          product.sub_category_id = product_sub_category.id
        end
        product.save!
      end
    end

    protected

    def set_initial_balance
      if quantity.present?
        self.initial_quantity = quantity
      end
    end
  end
end
