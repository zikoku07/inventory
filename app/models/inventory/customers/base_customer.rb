module Inventory
  module Customers
    class BaseCustomer < Inventory::Base
      self.abstract_class = true
      self.table_name_prefix = 'inventory_customers_'
    end
  end
end