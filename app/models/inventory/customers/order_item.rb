module Inventory
  module Customers
    class OrderItem < BaseCustomer
      belongs_to :order, :class_name => 'Inventory::Customers::Order', foreign_key: :order_id
      belongs_to :product, :class_name => 'Inventory::Product'
      after_create :set_product_value
      after_update :change_product_value, if: -> { quantity_changed? }
      after_destroy :reset_product_value

      protected

      def set_product_value
        product = Inventory::Product.find_by_id(product_id)
        qt = quantity.present? ? product.quantity - quantity : product.quantity
        product.update_attributes(quantity: qt)
      end

      def change_product_value
        product = Inventory::Product.find_by_id(product_id)
        qt = quantity_changed? && quantity.present? ? product.quantity - (quantity - quantity_was) : product.quantity
        product.update_attributes(quantity: qt)
      end

      def reset_product_value
        product = Inventory::Product.find_by_id(product_id)
        qt = product.quantity + quantity
        product.update_attributes(quantity: qt)
      end
    end
  end
end

