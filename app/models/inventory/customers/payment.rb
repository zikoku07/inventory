module Inventory
  module Customers
    class Payment < Inventory::Customers::BaseCustomer
      belongs_to :department
      belongs_to :party, :class_name => 'Inventory::Party'
      belongs_to :employee
      belongs_to :sales_order, :class_name => 'Inventory::Customers::Order', foreign_key: :order_id
      before_save :set_total
      after_save :change_party_out_balance
      after_destroy :reset_party_out_balance

      protected

      def set_total
        if amount.present? && !total.present?
          self.total = amount
        end
      end

      def change_party_out_balance
        party = Inventory::Party.find_by_id(party_id)
        if amount_was.present?
          party.update_attributes(out_balance: (party.out_balance + amount_was) - amount)
        else
          party.update_attributes(out_balance: party.out_balance - amount)
        end
      end

      def reset_party_out_balance
        party = Inventory::Party.find_by_id(party_id)
        party.update_attributes(out_balance: party.out_balance + amount)
      end
    end
  end
end
