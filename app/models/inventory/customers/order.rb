module Inventory
  module Customers
    class Order < BaseCustomer
      belongs_to :department
      belongs_to :employee
      belongs_to :party, :class_name => 'Inventory::Party'
      has_one :customers_payment, :class_name => 'Inventory::Customers::Payment', foreign_key: :order_id
      has_many :order_items, :class_name => 'Inventory::Customers::OrderItem'
      after_save :change_party_out_balance
      after_destroy :reset_party_out_balance
      before_save :check_amount_cash, if: -> { customers_payment.present? }
      accepts_nested_attributes_for :customers_payment,:allow_destroy => true,
                                    :reject_if => proc { |attributes| attributes['method'].blank?}
     accepts_nested_attributes_for :order_items,
                                    :allow_destroy => true,
                                    :reject_if => proc {|attributes|
                                      attributes.all? {|k,v| v.blank?}
                                    }

      def change_party_out_balance
        party = Inventory::Party.find_by_id(party_id)
        if total_was.present?
          party.update_attributes(out_balance: (party.out_balance - total_was) + total)
        else
          party.update_attributes(out_balance: party.out_balance + total)
        end
      end

      def reset_party_out_balance
        party = Inventory::Party.find_by_id(party_id)
        party.update_attributes(out_balance: party.out_balance - total)
      end

      def check_amount_cash
        if total == customers_payment.amount
          self.is_cash =  true
          customers_payment.is_cash = true
        end
        customers_payment.employee_id = employee_id
        customers_payment.party_id = party_id
        customers_payment.department_id = department_id
      end

    end
  end
end

