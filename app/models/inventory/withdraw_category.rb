module Inventory
  class WithdrawCategory < Inventory::Base
    belongs_to :department
    has_many :withdraws, :class_name => 'Inventory::Withdraw'
  end
end