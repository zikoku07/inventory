module Inventory
  class DepositCategory < Base
    belongs_to :department
    has_many :deposits, :class_name => 'Inventory::Deposit'
  end
end