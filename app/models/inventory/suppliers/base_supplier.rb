module Inventory
  module Suppliers
    class BaseSupplier < Inventory::Base
      self.abstract_class = true
      self.table_name_prefix = 'inventory_suppliers_'
    end
  end
end