module Inventory
  module Suppliers
    class OrderItem < BaseSupplier
      belongs_to :order, :class_name => 'Inventory::Suppliers::Order', foreign_key: :order_id
      belongs_to :product, :class_name => 'Inventory::Product'
      after_create :set_product_value
      after_update :change_product_value, if: -> { quantity_changed? || buying_price_changed? }
      after_destroy :reset_product_value

      protected

      def set_product_value
        product = Inventory::Product.find_by_id(product_id)
        bp = buying_price.present? ? buying_price : product.buying_price
        qt = quantity.present? ? product.quantity + quantity : product.quantity
        product.update_attributes(quantity: qt, buying_price: bp)
      end

      def change_product_value
        product = Inventory::Product.find_by_id(product_id)
        qt = quantity_changed? && quantity.present? ? product.quantity + (quantity - quantity_was) : product.quantity
        bp = buying_price_changed? && buying_price.present? ? buying_price : product.buying_price
        product.update_attributes(quantity: qt, buying_price: bp)
      end

      def reset_product_value
        product = Inventory::Product.find_by_id(product_id)
        qt = product.quantity - quantity
        product.update_attributes(quantity: qt)
      end
    end
  end
end

