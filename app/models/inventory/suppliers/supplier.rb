module Inventory
  module Suppliers
    class Supplier < BaseSupplier
      belongs_to :department
      has_many :orders, :class_name => 'Inventory::Suppliers::Order'

      def self.active
        self.where(is_active: true)
      end
    end
  end
end

