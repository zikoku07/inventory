module Inventory
  module Suppliers
    class Payment < Inventory::Suppliers::BaseSupplier
      belongs_to :department
      belongs_to :party, :class_name => 'Inventory::Party'
      belongs_to :employee
      belongs_to :purchase_order, :class_name => 'Inventory::Suppliers::Order', foreign_key: :order_id
      before_save :set_total
      after_save :change_party_in_balance
      after_destroy :reset_party_in_balance

      protected

      def set_total
        if amount.present? && !total.present?
          self.total = amount
        end
      end

      def change_party_in_balance
        party = Inventory::Party.find_by_id(party_id)
        if amount_was.present?
          party.update_attributes(in_balance: (party.in_balance + amount_was) - amount)
        else
          party.update_attributes(in_balance: party.in_balance - amount)
        end
      end

      def reset_party_in_balance
        party = Inventory::Party.find_by_id(party_id)
        party.update_attributes(in_balance: party.in_balance + amount)
      end
    end
  end
end
