module Inventory
  module Suppliers
    class Order < BaseSupplier
      belongs_to :department
      belongs_to :employee
      belongs_to :supplier, :class_name => 'Inventory::Suppliers::Supplier'
      belongs_to :party, :class_name => 'Inventory::Party'
      has_one :suppliers_payment, :class_name => 'Inventory::Suppliers::Payment', foreign_key: :order_id
      has_many :order_items, :class_name => 'Inventory::Suppliers::OrderItem', foreign_key: :order_id
      after_save :change_party_in_balance
      after_destroy :reset_party_in_balance
      before_save :check_amount_cash, if: -> { suppliers_payment.present? }
      accepts_nested_attributes_for :suppliers_payment,
                                    :allow_destroy => true,
                                    :reject_if => proc { |attributes| attributes['amount'].blank?}
      accepts_nested_attributes_for :order_items,
                                    :allow_destroy => true,
                                    :reject_if => proc {|attributes|
                                      attributes.all? {|k,v| v.blank?}
                                    }

      def change_party_in_balance
        party = Inventory::Party.find_by_id(party_id)

        if total_was.present?
          party.update_attributes(in_balance: (party.in_balance - total_was) + total)
        else
          party.update_attributes(in_balance: party.in_balance + total)
        end
      end

      def reset_party_in_balance
        party = Inventory::Party.find_by_id(party_id)
        party.update_attributes(in_balance: party.in_balance - total)
      end

      def check_amount_cash
      if total == suppliers_payment.amount
        self.is_cash =  true
        suppliers_payment.is_cash = true
      end
        suppliers_payment.employee_id = employee_id
        suppliers_payment.party_id = party_id
        suppliers_payment.department_id = department_id
      end

    end
  end
end

