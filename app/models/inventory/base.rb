module Inventory
  class Base < ActiveRecord::Base
    self.abstract_class = true
    self.table_name_prefix = 'inventory_'
  end
end