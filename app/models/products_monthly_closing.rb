class ProductsMonthlyClosing < ActiveRecord::Base
  belongs_to :department
  belongs_to :product, :class_name => 'Inventory::Product'

  def self.close(month = (Date.today - 1.day).month, year = (Date.today - 1.day).year)
    department = Department.first
    Inventory::Product.all.each do |product|
      department.products_monthly_closings.create(product_id: product.id, quantity: product.quantity, buying_price: product.buying_price, month: month, year: year)
    end
  end
end
