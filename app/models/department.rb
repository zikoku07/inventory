class Department < ActiveRecord::Base
  has_many :employees
  has_many :expenses_categories, :class_name => 'Expenses::Category'
  has_many :expenses, :class_name => 'Expenses::Expense'
  has_many :parties, :class_name => 'Inventory::Party'
  has_many :customers, :class_name => 'Inventory::Customers::Customer'
  has_many :suppliers, :class_name => 'Inventory::Suppliers::Supplier'
  has_many :products, :class_name => 'Inventory::Product'
  has_many :product_categories, :class_name => 'Inventory::ProductCategory'
  has_many :purchase_orders, :class_name => 'Inventory::Suppliers::Order'
  has_many :sale_orders, :class_name => 'Inventory::Customers::Order'
  has_many :suppliers_payments, :class_name => 'Inventory::Suppliers::Payment'
  has_many :customers_payments, :class_name => 'Inventory::Customers::Payment'
  has_many :deposit_categories, :class_name => 'Inventory::DepositCategory'
  has_many :deposits, :class_name => 'Inventory::Deposit'
  has_many :product_replacements, :class_name => 'Inventory::ProductReplacement'
  has_many :withdraw_categories, :class_name => 'Inventory::WithdrawCategory'
  has_many :withdraws, :class_name => 'Inventory::Withdraw'
  has_many :daily_closings
  has_many :products_monthly_closings
  has_many :parties_monthly_closings

  def year
    self.created_at.present? ? self.created_at.strftime('%Y').to_i : Date.today.year
  end

  def month
    self.created_at.present? ? self.created_at.strftime('%m').to_i : Date.today.month
  end

  def created_date
    self.created_at.present? ? self.created_at.strftime('%Y-%m-%d').to_date : Date.today
  end

  def self.get_daily_cash_outs(current_department, date)
    cash_outs = []
    supplier_cash_orders = current_department.purchase_orders.where(is_cash: true, date: date)
    expenses = current_department.expenses.where(date: date)
    deposits = current_department.deposits.where(date: date)
    withdraws = current_department.withdraws.where(date: date)

    supplier_cash_orders.each do |order|
      order.order_items.each do |order_item|
        cash_outs.push({name: order_item.product.name, quantity: order_item.quantity, amount: order_item.total, created_at: order_item.created_at})
      end
    end

    expenses.each do |expense|
      cash_outs.push({name: expense.expenses_category.name, quantity: '', amount: expense.amount, created_at: expense.created_at})
    end

    deposits.each do |deposit|
      cash_outs.push({name: deposit.deposit_category.name, quantity: '', amount: deposit.amount, created_at: deposit.created_at})
    end

    withdraws.each do |withdraw|
      cash_outs.push({name: withdraw.withdraw_category.name, quantity: '', amount: withdraw.amount, created_at: withdraw.created_at})
    end
    cash_outs.sort_by! {|data| data[:created_at]}
  end
end
