class Employee < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:user_login]

  attr_accessor :user_login

  mount_uploader :image, ImageUploader

  belongs_to :department
  has_many :suppliers_payments, :class_name => 'Inventory::Suppliers::Payment'
  has_many :customers_payments, :class_name => 'Inventory::Customers::Payment'
  has_many :suppliers_orders, :class_name => 'Inventory::Suppliers::Order'
  has_many :customers_orders, :class_name => 'Inventory::Customers::Order'

  def user_login(user_login)
    @user_login = user_login
  end

  def user_login
    @user_login || self.user_name || self.email
  end

  def update_without_password(params, *options)
    if params[:password].blank?
      params.delete(:password)
      params.delete(:password_confirmation) if params[:password_confirmation].blank?
    end
    result = update_attributes(params, *options)
    clean_up_passwords
    result
  end

  def email_required?
    false
  end

  def email_changed?
    false
  end

  protected

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:user_login)
      where(conditions.to_h).where(["lower(user_name) = :value OR lower(email) = :value", {:value => login.downcase}]).first
    elsif conditions.has_key?(:user_name) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end
end
