class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :authenticate_employee!
  helper_method :current_department, :active_parties

  def current_department
    if session[:department_id].present?
      department_id = session[:department_id]
    elsif current_employee.present? and current_employee.department_id.present?
      department_id = current_employee.department_id
    end
    @cur_department ||= Department.find_by_id(department_id)
    unless @cur_department.present?
      @cur_department = Department.first
    end
    @cur_department
  end

  def active_parties
    current_department.parties.active
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) << :userid
  end
end
