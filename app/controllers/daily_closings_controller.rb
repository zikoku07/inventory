class DailyClosingsController < ApplicationController
  before_action :set_daily_closing, only: [:destroy]

  def index
    @daily_closings = current_department.daily_closings
  end

  def create
    @daily_closing = current_department.daily_closings.build(daily_closing_params)
    respond_to do |format|
      if @daily_closing.save
        format.html {redirect_to cash_book_path(date: params[:daily_closing][:date]), notice: 'Successfully closed.'}
      else
        format.html {redirect_to cash_book_path(date: params[:daily_closing][:date]), notice: 'Error occured when closing the cash book. Please check your internet or try again later.'}
      end
    end
  end

  def destroy
    start_date = Date.parse(params[:date])
    end_date = Date.today
    @daily_closings = current_department.daily_closings.where(date: start_date..end_date)
    @daily_closings.each do |daily_closing|
      daily_closing.destroy!
    end
    respond_to do |format|
        format.html { redirect_to cash_book_path(date: params[:date]), notice: 'Closing successfully removed.' }
      end
  end

  private

  def set_daily_closing
    @daily_closing = DailyClosing.find_by_id(params[:id])
  end

  def daily_closing_params
    params.require(:daily_closing).permit!
  end
end
