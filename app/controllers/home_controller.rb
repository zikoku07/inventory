class HomeController < ApplicationController
  def index
    session[:department_id] = current_department.id
  end

  def cash_book
    @date = params[:date].present? ? Date.parse(params[:date]) : Date.today

    @customer_credit_orders = current_department.sale_orders.where(is_cash: false, date: @date)
    @supplier_credit_orders = current_department.purchase_orders.where(is_cash: false, date: @date)
    @customer_cash_orders = current_department.sale_orders.where(is_cash: true, date: @date)
    @customer_credit_payments = current_department.customers_payments.where(is_cash: false, date: @date)
    @supplier_credit_payments = current_department.suppliers_payments.where(is_cash: false, date: @date)

    @cash_outs = Department.get_daily_cash_outs(current_department, @date)

    @cash_in = 0.0
    @cash_out = 0.0

    @daily_closing = DailyClosing.find_by_date(@date)
    @tomorrow_closing = DailyClosing.find_by_date(@date - 1.day)

    if @tomorrow_closing.present?
      @initial_balance = @tomorrow_closing.balance
    else
      department_date = current_department.created_at.strftime('%Y-%m-%d').to_date
      if @date == department_date
        @initial_balance = current_department.balance
      end
    end

    unless @daily_closing.present?
      @daily_closing = DailyClosing.new
    end

    @is_monthly_closing = false
    unless @date.month == current_department.month
      closed_products = ProductsMonthlyClosing.where(month: (@date.beginning_of_month - 1.day).month, year: (@date.beginning_of_month - 1.day).year)
      @is_monthly_closing = closed_products.present? ? false : true
    end
  end

  def close_previous_month
    ProductsMonthlyClosing.close
    PartiesMonthlyClosing.close
    respond_to do |format|
      format.html{ redirect_to cash_book_path(), notice: 'Successfully closed.'}
    end
  end

  def daily_status
    date = Date.today


    #**********************Supplier************************#
    @suppliers_data = []
    purchase_orders = current_department.purchase_orders#.where(date: date)
    purchase_orders.each do |order|
      @suppliers_data.push({type: 'Order', id: order.id, party_id: order.party_id, party_name: order.party.name, amount: order.total, method: order.method, created_at: order.created_at})
    end
    suppliers_payments = current_department.suppliers_payments#.where(date: date)
    suppliers_payments.each do |payment|
      @suppliers_data.push({type: 'Payment', id: payment.id, party_id: payment.party_id, party_name: payment.party.name, amount: payment.amount, method: payment.method, created_at: payment.created_at})
    end
    @suppliers_data.sort_by! {|data| data[:created_at]}


    #**********************Customer************************#
    @customer_data = []
    sales_orders = current_department.sale_orders#.where(date: date)
    sales_orders.each do |order|
      @customer_data.push({type: 'Order', id: order.id, party_id: order.party_id, party_name: order.party.name, amount: order.total, method: order.method, created_at: order.created_at})
    end
    customers_payments = current_department.customers_payments#.where(date: date)
    customers_payments.each do |payment|
      @customer_data.push({type: 'Payment', id: payment.id, party_id: payment.party_id, party_name: payment.party.name, amount: payment.amount, method: payment.method, created_at: payment.created_at})
    end
    @customer_data.sort_by! {|data| data[:created_at]}


    # @suppliers_data.each do |data|
    #   puts data.inspect
    # end
  end
end
