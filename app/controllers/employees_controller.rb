class EmployeesController < ApplicationController
  before_action :set_employee, only: [:edit, :update_info, :destroy, :show, :update_password, :change_password]

  def index
    @employees = current_department.employees.where(is_active: true)
  end

  def show
    respond_to do |format|
      format.js
    end
  end

  def new
    @employee = Employee.new
  end

  def add
    @employee = current_department.employees.build(employee_params)
    @employee.role = AppSetting::EMPLOYEE_ROLE[:staff]
    respond_to do |format|
      if @employee.save
        format.html {redirect_to employees_path, notice: 'Employee successfully created'}
      else
        format.html { render :new, danger: "Employee couldn't be created" }
      end
    end
  end

  def edit
  end

  def update_info
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to employees_path, notice: "Employee was successfully created" }
      else
        format.html { redirect_to employees_path, notice: "Employee couldn't be updated" }
      end
    end
  end

  def change_password
  end

  def update_password
    respond_to do |format|
      if @employee.update_without_password(employee_params)
        if @employee == current_employee
          sign_out @employee
        end
        format.html { redirect_to employees_path, success: 'You have changed your password successfully.' }
      else
        format.html { redirect_to :back, notice: "#{ errors_to_message_string(@employee.errors) }" }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @employee == current_employee
        format.html { redirect_to employees_path, notice: 'You can not delete yourself.' }
      else
        @employee.destroy!
        format.html { redirect_to employees_path, notice: 'Successfully deleted' }
      end
    end
  end

  private
  def set_employee
    @employee = Employee.find_by_id(params[:id])
  end

  def employee_params
    params.require(:employee).permit!
  end
end
