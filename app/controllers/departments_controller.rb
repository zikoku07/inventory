class DepartmentsController < ApplicationController
  before_action :set_department, only: [:edit, :update, :destroy, :show, :initial_balance, :update_balance]

  def index
    @departments = Department.all
    respond_to do |format|
      format.html {}
    end
  end

  def new
    @department = Department.new
    respond_to do |format|
      format.js {}
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def create
    @department = Department.new(department_params)
    respond_to do |format|
      if @department.save
        format.html {redirect_to departments_path, notice: 'Department successfully created'}
      else
        format.html { render :new, danger: "Department couldn't be created" }
      end
    end
  end

  def show
    respond_to do |format|
      format.js
    end
  end

  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to departments_path, notice: "Department was successfully created" }
      else
        format.html { render :new }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @department == current_employee.department
        format.html { redirect_to departments_path, notice: 'You are in the department So you have to switch yourself to another department' }
      else
        @department.destroy!
        format.html { redirect_to departments_path, notice: 'Successfully deleted' }
      end
    end
  end

  def switch
    @department = Department.find_by_id(params[:id])
    session[:department_id] = @department.id if @department.present?
    if Rails.application.routes.recognize_path(request.referrer)[:controller] == 'departments' && Rails.application.routes.recognize_path(request.referrer)[:action] == 'show'
      redirect_to department_path(@department)
    else
      redirect_to request.referer
    end
  end

  def initial_balance
    @is_editable = current_department.daily_closings.count > 0 ? false : true
  end

  def update_balance
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to initial_balance_department_path(@department), notice: "Initial balance was successfully updated." }
      else
        format.html { redirect_to initial_balance_department_path(@department), notice: "Error occured when updating initial balance. Pleasr check your internet or try again later" }
      end
    end
  end

  private

  def set_department
    @department = Department.find_by_id(params[:id])
  end

  def department_params
    params.require(:department).permit!
  end
end
