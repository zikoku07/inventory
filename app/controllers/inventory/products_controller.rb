module Inventory
  class ProductsController < BaseController
    before_action :set_product, only: [:show, :edit, :update, :stock_report] #, :destroy

    def index
      @products = current_department.products.includes(:category).includes(:sub_category)
    end

    def show
    end

    def new
      @product = Inventory::Product.new
      @product_categories = current_department.product_categories.where(category_id: nil)
    end

    def create
      @product = current_department.products.build(product_params)
      respond_to do |format|
        if @product.save
          format.html {redirect_to inventory_products_path, notice: 'Product successfully created.' }
          format.js
          format.json
        else
          format.html {redirect_to inventory_products_path, notice: "Product couldn't be created."}
          format.js
          format.json
        end
      end
    end

    def edit
      @product_categories = current_department.product_categories.where(category_id: nil)
      if @product.category.present?
        @product_sub_categories = Inventory::ProductCategory.sub_categories(@product.category_id)
      end
      purchase_items = @product.suppliers_order_items
      sales_items = @product.customers_order_items
      @is_quantity_editable = purchase_items.present? || sales_items.present? ? false : true
    end

    def update
      respond_to do |format|
        if @product.update(product_params)
          format.html {redirect_to inventory_products_path, notice: 'Product info successfully updated.' }
          format.js
          format.json
        else
          format.html {redirect_to inventory_products_path, notice: "Product info couldn't be updated."}
          format.js
          format.json
        end
      end
    end

    # def destroy
    #   respond_to do |format|
    #     @product.destroy
    #     format.html {redirect_to inventory_products_path, notice: 'Product successfully deleted.' }
    #   end
    # end

    def import
      Inventory::Product.import(params[:file])
      redirect_to inventory_products_path, notice: "Products imported."
    end

    def get_details
      @product = Inventory::Product.find_by_id(params[:id])
      respond_to do |format|
        format.js
      end
    end

    def stock_report
      start_date = params[:date].present? ? Date.new(params[:date][:year].to_i, params[:date][:month].to_i, 1) : Date.today.beginning_of_month
      end_date = start_date.end_of_month
      @total_in = 0
      @total_out = 0
      
      if @product.created_date > end_date
        @message = "#{ @product.name } not present on that month"
      elsif @product.created_date.between?(start_date, end_date)
        @initial_stock = @product.initial_quantity
        @total_stock = @initial_stock
        @history = Inventory::Product.history(current_department, @product.id, start_date, end_date)
      else
        item = current_department.products_monthly_closings.where(product_id: @product.id, month: (start_date - 1.day).month, year: (start_date - 1.day).year)
        if item.present?
          @initial_stock = item.first.quantity
          @total_stock = @initial_stock
          @history = Inventory::Product.history(current_department, @product.id, start_date, end_date)
        else
          @message = "Initial stock not found. This occurred when you hadn't close your previous month's cash books."
        end
      end

      @products = current_department.products

      # if current_department.created_date > end_date
      #   redirect_to stock_report_inventory_products_path(id: @product.id), notice: 'Stock Not Found.'
      # elsif current_department.created_date < start_date
      #   closed_item_count = ProductsMonthlyClosing.where(month: (start_date - 1.day).month, year: (start_date - 1.day).year).count
      #   if closed_item_count > 0
      #     item = ProductsMonthlyClosing.where(product_id: @product.id, month: (start_date - 1.day).month, year: (start_date - 1.day).year)
      #     if item.present?
      #       @initial_stock = item.first.quantity
      #     end
      #   else
      #     @is_cal = false
      #   end
      # end

      # if @is_cal
      #   purchase_order_items = Inventory::Suppliers::OrderItem.where(product_id: @product.id).includes(:order).includes(:product).where('inventory_suppliers_orders.department_id = ? AND inventory_suppliers_orders.date IN (?)', current_department.id, start_date..end_date).references(:inventory_suppliers_orders)
      #   sales_order_items = Inventory::Customers::OrderItem.where(product_id: @product.id).includes(:order).includes(:product).where('inventory_customers_orders.department_id = ? AND inventory_customers_orders.date IN (?)', current_department.id, start_date..end_date).references(:inventory_customers_orders)
      #   replacements = current_department.product_replacements.where(product_id: @product.id).includes(:product).where(date: start_date..end_date)
      # 
      #   purchase_order_items.each do |order_item|
      #     @total_in += order_item.quantity
      #     @history.push({product: order_item.product.name, type: 'Purchase order', in: order_item.quantity, out: 0, date: order_item.order.date})
      #   end
      # 
      #   sales_order_items.each do |order_item|
      #     @total_out += order_item.quantity
      #     @history.push({product: order_item.product.name, type: 'Sales order', in: 0, out: order_item.quantity, date: order_item.order.date})
      #   end
      # 
      #   replacements.each do |replacement|
      #     @total_out += replacement.quantity
      #     @history.push({product: replacement.product.name, type: 'Replacement', in: 0, out: replacement.quantity, date: replacement.date})
      #     if replacement.receive_date.present? && replacement.receive_date.between?(start_date, end_date)
      #       @total_in += replacement.quantity
      #       @history.push({product: replacement.product.name, type: 'Replacement received', in: replacement.quantity, out: 0, date: replacement.date})
      #     end
      #   end
      #   @history.sort_by! {|history| history[:date]}
      #   @total_stock = @initial_stock
      # end
    end

    private
    def set_product
      @product = Inventory::Product.find_by_id(params[:id])
    end

    def product_params
      params.require(:inventory_product).permit!
    end
  end
end
