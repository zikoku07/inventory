module Inventory
  class ProductCategoriesController < BaseController
    before_action :set_product_category, only: [:edit, :update, :destroy, :show]

    def index
      @product_categories = Inventory::ProductCategory.get_all(current_department).includes(:sub_categories)
    end

    def show
      respond_to do |format|
        format.js
      end
    end

    def new
      @product_category = Inventory::ProductCategory.new
      respond_to do |format|
        format.js
      end
    end

    def create
      @product_category = current_department.product_categories.build(product_category_params)
      @product_category.save
      respond_to do |format|
        format.js
      end
    end

    def edit
      respond_to do |format|
        format.js
      end
    end

    def update
      @product_category.update(product_category_params)
      respond_to do |format|
        format.js
      end
    end

    # def destroy
    #   @product_category_id = @product_category.id
    #   @product_sub_category_id = nil
    #   if @product_category.category_id.present?
    #     @product_category_id = @product_category.category_id
    #     @product_sub_category_id = @product_category.id
    #   end
    #   @product_category.destroy
    #   respond_to do |format|
    #     format.js
    #   end
    # end

    def product_sub_categories
      @product_sub_categories = Inventory::ProductCategory.sub_categories(params[:id])
      puts @product_sub_categories.inspect
      respond_to do |format|
        format.js
      end
    end

    private
    def set_product_category
      @product_category = Inventory::ProductCategory.find_by_id(params[:id])
    end

    def product_category_params
      params.require(:inventory_product_category).permit!
    end
  end
end
