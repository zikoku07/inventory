module Inventory
  class DepositCategoriesController < BaseController
    before_action :set_deposit_category, only: [:edit, :update, :destroy, :show]

    def index
      @deposit_categories = current_department.deposit_categories
    end

    def show
      respond_to do |format|
        format.js
      end
    end

    def new
      @deposit_category = Inventory::DepositCategory.new
      respond_to do |format|
        format.js
      end
    end

    def create
      @deposit_category = current_department.deposit_categories.build(deposit_category_params)
      respond_to do |format|
        if @deposit_category.save
          format.html {redirect_to inventory_deposit_categories_path, notice: "Deposit Category has been created successfully"}
          format.js
        else
          format.html {render :new, notice: "Deposit Category couldn't be created"}
          format.js
        end
      end
    end

    def edit
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @deposit_category.update(deposit_category_params)
          format.html { redirect_to inventory_deposit_categories_path, notice: 'Deposit Category has been updated successfully'}
          format.js
        else
          format.html { render :new, notice: "Deposit Category couldn't be updated"}
          format.js
        end
      end
    end

    def destroy
      @deposit_category.destroy!
      respond_to do |format|
        format.html {redirect_to inventory_deposit_categories_path}
      end
    end

    private
    def set_deposit_category
      @deposit_category = Inventory::DepositCategory.find_by_id(params[:id])
    end

    def deposit_category_params
      params.require(:inventory_deposit_category).permit!
    end
  end
end
