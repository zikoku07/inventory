module Inventory
  class WithdrawCategoriesController < BaseController
    before_action :set_withdraw_category, only: [:edit, :update, :destroy, :show]

    def index
      @withdraw_categories = current_department.withdraw_categories
    end

    def show
      respond_to do |format|
        format.js
      end
    end

    def new
      @withdraw_category = Inventory::WithdrawCategory.new
      respond_to do |format|
        format.js
      end
    end

    def create
      @withdraw_category = current_department.withdraw_categories.build(withdraw_category_params)
      respond_to do |format|
        if @withdraw_category.save
          format.html {redirect_to inventory_withdraw_categories_path, notice: "Withdraw Category has been created successfully"}
          format.js
        else
          format.html {render :new, notice: "Withdraw Category couldn't be created"}
          format.js
        end
      end
    end

    def edit
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @withdraw_category.update(withdraw_category_params)
          format.html { redirect_to inventory_withdraw_categories_path, notice: 'Withdraw Category has been updated successfully'}
          format.js
        else
          format.html { render :new, notice: "Withdraw Category couldn't be updated"}
          format.js
        end
      end
    end

    def destroy
      @withdraw_category.destroy!
      respond_to do |format|
        format.html {redirect_to inventory_withdraw_categories_path}
      end
    end

    private
    def set_withdraw_category
      @withdraw_category = Inventory::WithdrawCategory.find_by_id(params[:id])
    end

    def withdraw_category_params
      params.require(:inventory_withdraw_category).permit!
    end
  end
end
