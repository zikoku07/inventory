module Inventory
  module Customers
    class CustomersController < Inventory::Customers::BaseController
      before_action :set_customer, only: [:show, :edit, :update, :destroy]

      def index
        @customers = current_department.customers.where(is_active: true)
      end

      def show
      end

      def new
        @customer = Inventory::Customers::Customer.new
        respond_to do |format|
          format.js
          format.html
        end
      end

      def create
        @customer = current_department.customers.build(customer_params)
        respond_to do |format|
          if @customer.save
            format.html {redirect_to inventory_customers_customers_path, notice: 'Customer successfully created.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_customers_customers_path, notice: "Customer couldn't be created."}
            format.js
            format.json
          end
        end
      end

      def edit
        respond_to do |format|
          format.js
        end
      end

      def update
        respond_to do |format|
          if @customer.update(customer_params)
            format.html {redirect_to inventory_customers_customers_path, notice: 'Customer info successfully updated.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_customers_customers_path, notice: "Customer info couldn't be updated."}
            format.js
            format.json
          end
        end
      end

      def destroy
        respond_to do |format|
          @customer.destroy
          format.html {redirect_to inventory_customers_customers_path, notice: 'Customer successfully deleted.' }
        end
      end

      private
      def set_customer
        @customer = Inventory::Customers::Customer.find_by_id(params[:id])
      end

      def customer_params
        params.require(:inventory_customers_customer).permit!
      end
    end
  end
end
