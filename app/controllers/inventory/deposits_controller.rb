module Inventory
  class DepositsController < BaseController
    before_action :set_deposit, only: [:show, :edit, :update, :destroy]

    def index
      @start_date = Date.today.beginning_of_month
      @end_date = Date.today.end_of_month
      if params[:daterange].present?
        date_range = params[:daterange].split('To')
        @start_date = Date.parse(date_range.first)
        @end_date = Date.parse(date_range.last)
      end
      @deposits = current_department.deposits.where(date: @start_date..@end_date)
    end

    def show
    end

    def new
      @deposit = Inventory::Deposit.new
      @deposit_categories = current_department.deposit_categories
      respond_to do |format|
        format.js
      end
    end

    def create
      @deposit = current_department.deposits.build(deposit_params)
      respond_to do |format|
        if @deposit.save
          format.html {redirect_to inventory_deposits_path, notice: 'Deposit successfully created.' }
          format.js
        else
          format.html {redirect_to inventory_deposits_path, notice: "Deposit couldn't be created."}
          format.js
        end
      end
    end

    def edit
      @deposit_categories = current_department.deposit_categories
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @deposit.update(deposit_params)
          format.html {redirect_to inventory_deposits_path, notice: 'Deposit info successfully updated.' }
          format.js
        else
          format.html {redirect_to inventory_deposits_path, notice: "Deposit info couldn't be updated."}
          format.js
        end
      end
    end

    def destroy
      respond_to do |format|
        @deposit.destroy
        format.html {redirect_to inventory_deposits_path, notice: 'Deposit successfully deleted.' }
      end
    end

    private
    def set_deposit
      @deposit = Inventory::Deposit.find_by_id(params[:id])
    end

    def deposit_params
      params.require(:inventory_deposit).permit!
    end
  end
end
