module Inventory
  class ProductReplacementsController < Inventory::BaseController
    before_action :set_product_replacement, only: [:show, :edit, :update, :destroy, :receive, :reject_product]

    def index
      @start_date = Date.today.beginning_of_month
      @end_date = Date.today.end_of_month
      if params[:daterange].present?
        date_range = params[:daterange].split('To')
        @start_date = Date.parse(date_range.first)
        @end_date = Date.parse(date_range.last)
      end
      @product_replacements = current_department.product_replacements.where(date: @start_date..@end_date).order(id: :desc)
    end

    def show
    end

    def new
      @product_replacement = Inventory::ProductReplacement.new
    end

    def create
      @product_replacement = current_department.product_replacements.build(product_replacement_params)
      respond_to do |format|
        if @product_replacement.save
          format.html {redirect_to inventory_product_replacements_path, notice: 'Product successfully sent to replace.' }
          format.js
          format.json
        else
          format.html {redirect_to inventory_product_replacements_path, notice: "Product couldn't be sent to replace."}
          format.js
          format.json
        end
      end
    end

    def edit
    end

    def update
      respond_to do |format|
        if @product_replacement.update(product_replacement_params)
          format.html {redirect_to inventory_product_replacements_path, notice: 'Product replacement info successfully updated.' }
          format.js
          format.json
        else
          format.html {redirect_to inventory_product_replacements_path, notice: "Product replacement info couldn't be updated."}
          format.js
          format.json
        end
      end
    end

    def destroy
      respond_to do |format|
        @product_replacement.destroy
        format.html {redirect_to inventory_product_replacements_path, notice: 'Product replacement info successfully deleted.' }
      end
    end

    def receive
      respond_to do |format|
        if @product_replacement.update_attributes(receive_date: Date.today)
          @product_replacement.product.update_attributes(quantity: @product_replacement.product.quantity + @product_replacement.quantity)
          format.html {redirect_to inventory_product_replacements_path, notice: 'Product replaced.' }
        else
          format.html {redirect_to inventory_product_replacements_path, notice: 'Product not replaced. Try again later' }
        end
      end
    end

    def reject_product
      respond_to do |format|
        if @product_replacement.update_attributes(receive_date: nil)
          @product_replacement.product.update_attributes(quantity: @product_replacement.product.quantity - @product_replacement.quantity)
          format.html {redirect_to inventory_product_replacements_path, notice: 'Replacement rejected.' }
        else
          format.html {redirect_to inventory_product_replacements_path, notice: 'Replacement not rejected. Try again later' }
        end
      end
    end

    private
    def set_product_replacement
      @product_replacement = Inventory::ProductReplacement.find_by_id(params[:id])
    end

    def product_replacement_params
      params.require(:inventory_product_replacement).permit!
    end
  end
end
