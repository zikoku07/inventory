module Inventory
  class PartiesController < BaseController
    before_action :set_party, only: [:show, :edit, :update, :destroy, :balance_report]

    def index
      @parties = current_department.parties.where(is_active: true)
    end

    def show
    end

    def new
      @party = Inventory::Party.new
      respond_to do |format|
        format.js
        format.html
      end
    end

    def create
      @party = current_department.parties.build(party_params)
      respond_to do |format|
        if @party.save
          format.html {redirect_to inventory_parties_path, notice: 'Party successfully created.'}
          format.js
          format.json
        else
          format.html {redirect_to inventory_parties_path, notice: "Party couldn't be created."}
          format.js
          format.json
        end
      end
    end

    def edit
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @party.update(party_params)
          format.html {redirect_to inventory_parties_path, notice: 'Party info successfully updated.' }
          format.js
          format.json
        else
          format.html {redirect_to inventory_parties_path, notice: "Party info couldn't be updated."}
          format.js
          format.json
        end
      end
    end

    # def destroy
    #   respond_to do |format|
    #     @party.destroy
    #     format.html {redirect_to inventory_parties_path, notice: 'Party successfully deleted.' }
    #   end
    # end

    def import
      Inventory::Party.import(current_department, params[:file])
      redirect_to inventory_parties_path, notice: 'Parties imported.'
    end

    def balance_report
      start_date = params[:date].present? ? Date.new(params[:date][:year].to_i, params[:date][:month].to_i, 1) : Date.today.beginning_of_month
      end_date = start_date.end_of_month
      @total_in = 0
      @total_out = 0

      if @party.created_date > end_date
        @message = "#{ @party.name } not present on that month"
      elsif @party.created_date.between?(start_date, end_date)
        @initial_balance = @party.initial_in_balance - @party.initial_out_balance
        @total_balance = @initial_balance
        @history = Inventory::Party.history(current_department, @party.id, start_date, end_date)
      else
        item = current_department.parties_monthly_closings.where(party_id: @party.id, month: (start_date - 1.day).month, year: (start_date - 1.day).year)
        if item.present?
          @initial_balance = item.first.balance
          @total_balance = @initial_balance
          @history = Inventory::Party.history(current_department, @party.id, start_date, end_date)
        else
          @message = "Initial balance not found. This occurred when you hadn't close your previous month's cash books."
        end
      end

      @parties = current_department.parties
    end

    private
    def set_party
      @party = Inventory::Party.find_by_id(params[:id])
    end

    def party_params
      params.require(:inventory_party).permit!
    end
  end
end
