module Inventory
  class WithdrawsController < BaseController
    before_action :set_withdraw, only: [:show, :edit, :update, :destroy]

    def index
      @start_date = Date.today.beginning_of_month
      @end_date = Date.today.end_of_month
      if params[:daterange].present?
        date_range = params[:daterange].split('To')
        @start_date = Date.parse(date_range.first)
        @end_date = Date.parse(date_range.last)
      end
      @withdraws = current_department.withdraws.where(date: @start_date..@end_date)
    end

    def show
    end

    def new
      @withdraw = Inventory::Withdraw.new
      @withdraw_categories = current_department.withdraw_categories
      respond_to do |format|
        format.js
      end
    end

    def create
      @withdraw = current_department.withdraws.build(withdraw_params)
      respond_to do |format|
        if @withdraw.save
          format.html {redirect_to inventory_withdraws_path, notice: 'Withdraw successfully created.' }
          format.js
        else
          format.html {redirect_to inventory_withdraws_path, notice: "Withdraw couldn't be created."}
          format.js
        end
      end
    end

    def edit
      @withdraw_categories = current_department.withdraw_categories
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @withdraw.update(withdraw_params)
          format.html {redirect_to inventory_withdraws_path, notice: 'Withdraw info successfully updated.' }
          format.js
        else
          format.html {redirect_to inventory_withdraws_path, notice: "Withdraw info couldn't be updated."}
          format.js
        end
      end
    end

    def destroy
      respond_to do |format|
        @withdraw.destroy
        format.html {redirect_to inventory_withdraws_path, notice: 'Withdraw successfully deleted.' }
      end
    end

    private
    def set_withdraw
      @withdraw = Inventory::Withdraw.find_by_id(params[:id])
    end

    def withdraw_params
      params.require(:inventory_withdraw).permit!
    end
  end
end
