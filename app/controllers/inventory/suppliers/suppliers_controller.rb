module Inventory
  module Suppliers
    class SuppliersController < Inventory::Suppliers::BaseController
      before_action :set_supplier, only: [:show, :edit, :update, :destroy]

      def index
        @suppliers = current_department.suppliers.where(is_active: true)
      end

      def show
      end

      def new
        @supplier = Inventory::Suppliers::Supplier.new
        respond_to do |format|
          format.js
          format.html
        end
      end

      def create
        @supplier = current_department.suppliers.build(supplier_params)
        respond_to do |format|
          if @supplier.save
            format.html {redirect_to inventory_suppliers_suppliers_path, notice: 'Supplier successfully created.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_suppliers_path, notice: "Supplier couldn't be created."}
            format.js
            format.json
          end
        end
      end

      def edit
        respond_to do |format|
          format.js
        end
      end

      def update
        respond_to do |format|
          if @supplier.update(supplier_params)
            format.html {redirect_to inventory_suppliers_suppliers_path, notice: 'Supplier info successfully updated.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_suppliers_path, notice: "Supplier info couldn't be updated."}
            format.js
            format.json
          end
        end
      end

      def destroy
        respond_to do |format|
          @supplier.destroy
          format.html {redirect_to inventory_suppliers_suppliers_path, notice: 'Supplier successfully deleted.' }
        end
      end

      private
      def set_supplier
        @supplier = Inventory::Suppliers::Supplier.find_by_id(params[:id])
      end

      def supplier_params
        params.require(:inventory_suppliers_supplier).permit!
      end
    end
  end
end
