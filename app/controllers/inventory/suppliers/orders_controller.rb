module Inventory
  module Suppliers
    class OrdersController < Inventory::Suppliers::BaseController
      before_action :set_order, only: [:show, :edit, :update, :destroy]

      def index
        @parties = current_department.parties
        @start_date = Date.today.beginning_of_month
        @end_date = Date.today.end_of_month
        if params[:daterange].present?
          date_range = params[:daterange].split('To')
          @start_date = Date.parse(date_range.first)
          @end_date = Date.parse(date_range.last)
        end
        @orders = current_department.purchase_orders.where(date: @start_date..@end_date)
        if params[:party_id].present?
          @orders = @orders.where(party_id: params[:party_id])
        end

        respond_to do |format|
          format.html {}
          format.xls
          format.pdf do
            render pdf: 'report', layout: 'pdf', template: 'inventory/suppliers/orders/purchaseorder_list_pdf.html.erb', encoding: 'utf-8'
          end
        end
      end

      def new
        @order = Inventory::Suppliers::Order.new
        @order.build_suppliers_payment(department_id: current_department.id)
        @parties = Inventory::Party.active
        respond_to do |format|
          format.html
        end
      end

      def create
        @order = current_department.purchase_orders.build(order_params)
        @order.employee_id = current_employee.id
        respond_to do |format|
          if @order.save
            format.html {redirect_to inventory_suppliers_orders_path, notice: 'Purchase Order successfully created.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_orders_path, notice: "Purchase order couldn't be created."}
            format.js
            format.json
          end
        end
      end

      def show

      end

      def edit
        @parties = Inventory::Party.active
        respond_to do |format|
          format.html
        end
      end

      def update
        respond_to do |format|
          if @order.update(order_params)
            format.html {redirect_to inventory_suppliers_orders_path, notice: 'Purchase order info successfully updated.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_orders_path, notice: "Purchase order info couldn't be updated."}
            format.js
            format.json
          end
        end
      end

      def destroy
        respond_to do |format|
          @order.destroy
          format.html {redirect_to inventory_suppliers_orders_path, notice: 'Purchase order successfully deleted.' }
        end
      end

      private
      def set_order
        @order = Inventory::Suppliers::Order.find_by_id(params[:id])
      end

      def order_params
        params.require(:inventory_suppliers_order).permit!
      end
    end
  end
end
