module Inventory
  module Suppliers
    class PaymentsController < Inventory::Suppliers::BaseController
      before_action :set_suppliers_payment, only: [:show, :edit, :update, :destroy]

      def index
        @start_date = Date.today.beginning_of_month
        @end_date = Date.today.end_of_month
        if params[:daterange].present?
          date_range = params[:daterange].split('To')
          @start_date = Date.parse(date_range.first)
          @end_date = Date.parse(date_range.last)
        end
        @payments = current_department.suppliers_payments.where(date: @start_date..@end_date)
        if params[:party_id].present?
          @payments = @payments.where(party_id: params[:party_id])
        end

        respond_to do |format|
          format.html {}
          format.xls
          format.pdf do
            render pdf: 'report', layout: 'pdf', template: 'inventory/suppliers/payments/supplier_payment_list_pdf.html.erb', encoding: 'utf-8'
          end
        end
      end

      def show
      end

      def new
        @payment = Inventory::Suppliers::Payment.new
      end

      def create
        @payment = current_department.suppliers_payments.build(suppliers_payment_params)
        @payment.employee_id = current_employee.id
        respond_to do |format|
          if @payment.save
            format.html {redirect_to inventory_suppliers_payments_path, notice: 'Payment successfully created.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_payments_path, notice: "Payment couldn't be created."}
            format.js
            format.json
          end
        end
      end

      def edit
      end

      def update
        respond_to do |format|
          if @payment.update(suppliers_payment_params)
            format.html {redirect_to inventory_suppliers_payments_path, notice: 'Payment successfully updated.' }
            format.js
            format.json
          else
            format.html {redirect_to inventory_suppliers_payments_path, notice: "Payment couldn't be updated."}
            format.js
            format.json
          end
        end
      end

      def destroy
        respond_to do |format|
          @payment.destroy
          format.html {redirect_to inventory_suppliers_payments_path, notice: 'Payment successfully deleted.' }
        end
      end

      private
      def set_suppliers_payment
        @payment = Inventory::Suppliers::Payment.find_by_id(params[:id])
      end

      def suppliers_payment_params
        params.require(:inventory_suppliers_payment).permit!
      end
    end
  end
end
