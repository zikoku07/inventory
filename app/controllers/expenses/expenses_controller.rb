module Expenses
  class ExpensesController < BaseController
    before_action :set_expense, only: [:show, :edit, :update, :destroy]

    def index
      @start_date = Date.today.beginning_of_month
      if params[:date].present?
        @start_date = Date.new(params[:date][:year].to_i, params[:date][:month].to_i, 1)
      end
      @end_date = @start_date.end_of_month

      @expenses = current_department.expenses.where(date: @start_date..@end_date).order(id: :desc)
      if params[:cat_id].present?
        @expenses = @expenses.where(category_id: params[:cat_id])
      end

      respond_to do |format|
        format.html {}
        format.xls
        format.pdf do
          render pdf: 'report', layout: 'pdf', template: 'expenses/expenses/expense_list_pdf.html.erb', encoding: 'utf-8'
        end
      end
    end

    def show
    end

    def new
      @expense = Expenses::Expense.new
      @categories = current_department.expenses_categories
      respond_to do |format|
        format.js
        format.html
      end
    end

    def create
      @expense = current_department.expenses.build(expense_params)
      respond_to do |format|
        if @expense.save
          format.html {redirect_to expenses_expenses_path, notice: 'Expense successfully created.' }
          format.js
          format.json
        else
          format.html {redirect_to expenses_expenses_path, notice: "Expense couldn't be created."}
          format.js
          format.json
        end
      end
    end

    def edit
      @categories = current_department.expenses_categories
      respond_to do |format|
        format.js
      end
    end

    def update
      respond_to do |format|
        if @expense.update(expense_params)
          format.html {redirect_to expenses_expenses_path, notice: 'Expense successfully updated.' }
          format.js
          format.json
        else
          format.html {redirect_to expenses_expenses_path, notice: "Expense couldn't be updated."}
          format.js
          format.json
        end
      end
    end

    def destroy
      respond_to do |format|
        @expense.destroy
        format.html {redirect_to expenses_expenses_path, notice: 'Expense successfully deleted.' }
      end
    end

    def monthly_report
      @start_date = Date.today.beginning_of_month
      if params[:date].present?
        @start_date = Date.new(params[:date][:year].to_i, params[:date][:month].to_i, 1)
      end
      @end_date = @start_date.end_of_month
      @expense_categories = current_department.expenses_categories
      @expense_report = Expenses::Expense.get_expense_report(@expense_categories, current_department, @start_date, @end_date, true)
    end

    def yearly_report
      @start_date = params[:date].present? ? Date.new(params[:date][:year].to_i, 1, 1) : Date.today.beginning_of_year
      @end_date = @start_date.end_of_year
      @expense_categories = current_department.expenses_categories
      @expense_report = Expenses::Expense.get_expense_report(@expense_categories, current_department, @start_date, @end_date, false)
    end

    def report_breakdown
      @start_date = params[:start_date].present? ? params[:start_date].to_date : Date.today
      @end_date = params[:end_date].present? ? params[:end_date].to_date : Date.today
      @expenses = current_department.expenses.where(date: @start_date..@end_date)
      if params[:category_id].present?
        @expenses = @expenses.where(category_id: params[:category_id])
      end
    end

    private
    def set_expense
      @expense = Expenses::Expense.find_by_id(params[:id])
    end

    def expense_params
      params.require(:expenses_expense).permit!
    end
  end
end
