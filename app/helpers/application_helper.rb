module ApplicationHelper
  def departments
    Department.all
  end

  def flash_types
    ['info', 'success', 'danger', 'warning']
  end

  # 1 Januarry, 2017
  def date_format_1(date)
    date.strftime('%d %B, %Y') if date.present?
  end

  # 1 Jan, 2017
  def date_format_2(date)
    date.strftime('%d %b, %Y') if date.present?
  end

  # 1 Jan
  def date_format_3(date)
    date.strftime('%d %b') if date.present?
  end
end
